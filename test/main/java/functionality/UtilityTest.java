package main.java.functionality;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {

    Utility util = new Utility();

    int[] cardNumberValid = util.getCardNumber("4242424242424240");
    int[] cardNumberInvalid = util.getCardNumber("4242424242424242");

    @Test
    void displayCardNumberAndCheckDigitOK() {
        String expected = "424242424242424 0";
        assertEquals(expected, util.displayCardNumberAndCheckDigit(cardNumberValid));
    }

    @Test
    void displayCardNumberAndCheckDigitNotOK() {
        String expected = "424242424242424 0";
        assertNotEquals(expected, util.displayCardNumberAndCheckDigit(cardNumberInvalid));
    }

    @Test
    void getExpectedControlNumberOK() {
        assertEquals(0, util.getExpectedControlNumber(cardNumberValid));
    }

    @Test
    void getExpectedControlNumberNotOK() {
        assertEquals(0, util.getExpectedControlNumber(cardNumberInvalid));
    }

    @Test
    void getProvidedCheckDigitOK() {
        assertEquals(0, util.getProvidedCheckDigit(cardNumberValid));
    }

    @Test
    void getProvidedCheckDigitNotOK() {
        assertNotEquals(0, util.getProvidedCheckDigit(cardNumberInvalid));
    }

    @Test
    void sumOfMultipliedDigitsOK() {
        assertEquals(28, util.sumOfMultipliedDigits(cardNumberValid));
    }

    @Test
    void sumOfMultipliedDigitsNotOK() {
        assertNotEquals(29, util.sumOfMultipliedDigits(cardNumberValid));
    }

    @Test
    void sumOfNormalDigitsOK() {
        assertEquals(32, util.sumOfNormalDigits(cardNumberValid));
    }

    @Test
    void sumOfNormalDigitsNotOK() {
        assertNotEquals(33, util.sumOfNormalDigits(cardNumberValid));
    }

    @Test
    void checkIfValidOK() {
        assertTrue(util.checkIfValid(cardNumberValid));
    }

    @Test
    void checkIfValidNotOK() {
        assertFalse(util.checkIfValid(cardNumberInvalid));
    }

    @Test
    void getLuhnSumOK() {
        int provided = util.getLuhnSum(cardNumberValid);
        assertEquals(60, provided);
    }

    @Test
    void getLuhnSumNotOK() {
        int provided = util.getLuhnSum(cardNumberValid);
        assertNotEquals(65, provided);
    }

    @Test
    void getNumberOfDigitsOK() {
        int provided = util.getNumberOfDigits(cardNumberValid);
        assertEquals(16, provided);
    }

    @Test
    void getNumberOfDigitsNotOK() {
        int provided = util.getNumberOfDigits(cardNumberValid);
        assertNotEquals(10, provided);
    }

}