package main.java.functionality;

import java.io.IOException;
import java.util.Scanner;

public class Utility {

    // Starting the console application.
    public void startProgram() {
        String userInput = getUserInput();
        int[] cardNumber = getCardNumber(userInput);
        displayResults(cardNumber);
    }

    // Placing the user input into an array of digits.
    public int[] getCardNumber(String cardNumber) {
        int[] digits = new int[cardNumber.length()];

        for(int i = 0; i < cardNumber.length(); i++) {
            digits[i] = cardNumber.charAt(i) - '0';
        }
        return digits;
    }

    // Accepts interaction with the console and returns the result
    public String getUserInput() {
        System.out.println("Enter a card number");
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.nextLine();
        } catch(Exception e) {
            throw new NumberFormatException();
        }
    }

    // Checking if the given card number is valid
    public void displayResults(int[] cardNumber) {
        System.out.println("\nRESULTS------------------");
        System.out.println("Input: " + displayCardNumberAndCheckDigit(cardNumber));
        System.out.println("Provided: " + getProvidedCheckDigit(cardNumber));
        System.out.println("Expected: " + getExpectedControlNumber(cardNumber));
        if(checkIfValid(cardNumber)) {
            System.out.println("\nChecksum: Valid");
        } else {
            System.out.println("\nChecksum: Invalid");
        }
        if(getNumberOfDigits(cardNumber) == 16) {
            System.out.println("Number of digits: " + getNumberOfDigits(cardNumber) + "(credit card)");
        } else {
            System.out.println("Number of digits: " + getNumberOfDigits(cardNumber) + "(not a credit card)");
        }
    }

    // Displaying the user input with the check digit separated.
    public String displayCardNumberAndCheckDigit(int[] cardNumber) {
        String out = "";
        for (int i = 0; i < cardNumber.length - 1; i++) {
            out += cardNumber[i];
        }
        out += " " + getProvidedCheckDigit(cardNumber);
        return out;
    }

    // Getting the control number by calculation
    public int getExpectedControlNumber(int[] cardNumber) {
        int sum = sumOfMultipliedDigits(cardNumber) + sumOfNormalDigits(cardNumber);
        return ((sum*9) % 10);
    }
    // Getting the control number by user input
    public int getProvidedCheckDigit(int[] cardNumber) {
        return cardNumber[cardNumber.length-1];
    }

    // Looping through the card number to get multiplied values. Excluding the control digit.
    public int sumOfMultipliedDigits(int[] cardNumber) {
        int sum = 0;

        for(int i = 1; i <= cardNumber.length - 2; i+=2) {
            if(cardNumber[i]*2 > 10) {
                sum += (cardNumber[i]*2) - 9;
            }
            sum += (cardNumber[i]*2);
        }
        return sum;
    }

    // Looping through the card number getting every other element from first index
    public int sumOfNormalDigits(int[] cardNumber) {
        int sum = 0;
        for(int i = 0; i < cardNumber.length; i+=2) {
            sum += cardNumber[i];
        }
        return sum;
    }

    // Checking if the card number is meeting the criteria of Luhn's Algorithm
    public Boolean checkIfValid(int[] cardNumber) {
        if(getLuhnSum(cardNumber) % 10 == 0) {
            return true;
        } else {
            return false;
        }
    }

    // Getting the total sum of the numbers
    public int getLuhnSum(int[] cardNumber) {
        int controlNumber = getProvidedCheckDigit(cardNumber);
        int sumOfNormalDigits = sumOfNormalDigits(cardNumber);
        int sumOfMultipliedDigits = sumOfMultipliedDigits(cardNumber);

        return sumOfNormalDigits + sumOfMultipliedDigits + controlNumber;
    }

    public int getNumberOfDigits(int[] cardNumber) {
        return cardNumber.length;
    }
}