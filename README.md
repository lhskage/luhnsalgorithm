### Luhns Algorithm

#### Description of Luhn's algorithm
https://en.wikipedia.org/wiki/Luhn_algorithm <br/> 
"The algorithm was designed to protect against accidental errors, not malicious attacks. 
Most credit cards and many government identification numbers use the algorithm as a simple
method of distinguishing valid numbers from mistyped or otherwise incorrect numbers."

#### Description of the application
The application provides a way to check if a number is meeting the criteria of Luhn's Algorithm. 
When running the application the user is prompted for a number. The number is then validated using
Luhn's Algorithm. 

#### Running the application
Retrieve the repository from gitlab using the following command:
```
$ git clone https://gitlab.com/lhskage/luhnsalgorithm
```
Navigate to the correct folder and run the application
```
$ cd LuhnAlgorithm\out\production\LuhnAlgorithm

$ java Program
```

#### Improvements
The applications in it's current state is missing input validation and error handling.

#### Tests
There are 8 different methods being tested and each method is tested using correct and
incorrect values. 